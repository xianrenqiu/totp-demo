import string
import paho.mqtt.client as mqtt  
import RPi.GPIO as GPIO  
import json
import ctypes
from ctypes import *   

def gpio_setup():  
    GPIO.setmode(GPIO.BCM)    
    GPIO.setup(17, GPIO.OUT)  
    GPIO.output(17, GPIO.LOW)  
          
def gpio_destroy():   
    GPIO.output(17, GPIO.LOW)  
    GPIO.setup(17, GPIO.IN)  
          
def on_connect(client, userdata, flags, rc):  
    print("Connected with result code " + str(rc))  
    client.subscribe("totp-demo")  
  
def on_message(client, userdata, msg):  
    print(msg.topic+" "+str(msg.payload))  
    totp = json.loads(str(msg.payload))  
	
    f = open('totp.txt','r')
    totp_data = f.read()
    totp_temp = string.atoi(totp_data)

    if totp['totp'] == totp_temp:
        print("totp is right!")
        if totp['lock'] == 1: 
            print("Lock!")
	    GPIO.output(17, GPIO.LOW)
        else:  
  	    print("UnLock!")
            GPIO.output(17, GPIO.HIGH)  
    else:
        print("totp error!")
	
if __name__ == '__main__':  
    client = mqtt.Client()  
    client.on_connect = on_connect  
    client.on_message = on_message  
    gpio_setup()  
      
    try:  
        client.connect("10.13.6.48", 1883, 60)
        client.loop_forever()  
    except KeyboardInterrupt:  
        client.disconnect()  
        gpio_destroy()  

