/* test.c */

#include <totp.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char **argv, char **env)
{
#ifdef HMAC-SHA-TEST
    printf("***** HMAC-SHA TEST *****\n");

    hash_t *hash;

    hash = (hash_t *)malloc(sizeof(hash_t) * MAX_LEN);

    char *msg = "testmsghellofoobar";

    size_t hash_len;

    int i;

    hash_len = hmac_sha1((hash_t *)key, (hash_t *)msg, hash);
    printf("SHA1(%s, %s) (len = %ld)\n= ", key, msg, hash_len);
    for (i = 0; i < hash_len; i++) printf("%02x", hash[i]);
    printf("\n");

    hash_len = hmac_sha256((hash_t *)key, (hash_t *)msg, hash);
    printf("SHA256(%s, %s) (len = %ld)\n= ", key, msg, hash_len);
    for (i = 0; i < hash_len; i++) printf("%02x", hash[i]);
    printf("\n");

    hash_len = hmac_sha512((hash_t *)key, (hash_t *)msg, hash);
    printf("SHA512(%s, %s) (len = %ld)\n= ", key, msg, hash_len);
    for (i = 0; i < hash_len; i++) printf("%02x", hash[i]);
    printf("\n");

    free(hash);
#endif
    printf("\n***** TOTP TEST *****\n");
    char *key = "xjxkwytchpymycj";
    int fd = 0;
    time_t t;
    char *token;
    size_t len;
    len = 6;
    
    struct tm *tmutc;
    
    t = time(NULL);
    tmutc = gmtime(&t); 
    printf("sleep %d s\r\n", 60-tmutc->tm_sec);
    sleep(60-tmutc->tm_sec);

    token = (char *)malloc(sizeof(char) * len);

    while(1)
    {
        t = time(NULL);
        printf("%s",asctime(gmtime(&t)));
        totp_hmac_sha1(key, t, len, token);
        //totp_hmac_sha256(key, t, len, token);
        //totp_hmac_sha512(key, t, len, token);
        *(token+6)=0;
        printf("sha1(%s):   %s\n", key, token);

        fd = open("totp.txt", O_WRONLY|O_CREAT);
        if(fd < 0)
        {
            printf("open file failed!\r\n");
        }

        write(fd, token, 6) ;
        close(fd); 
        sleep(60);
    }
    
    free(token);
    
    return 0;
}