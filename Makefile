CC = cc

OPENSSL_CFLAGS = $(shell pkg-config --libs openssl)

libtotp.so:
	$(CC) -c -Wall -fPIC -O2 src/totp.c
	$(CC) -shared -o libtotp.so totp.o $(OPENSSL_CFLAGS)

all: libtotp.so 

clean:
	rm -rf totp.o libtotp.so
	rm -rf example/generate_totp

generate_totp: all
	cp libtotp.so /usr/lib/
	$(CC) -Wall -o example/generate_totp example/generate_totp.c -L./ -ltotp -Isrc/

.PHONY: all clean generate_totp